from django.db import models
from users.models import User


#Author : Debojit Kaushik ( 26/3/2017 )

#Class for society.
class Society(models.Model):

    '''
        Database model for Society.
    '''

    society_id = models.UUIDField(
        primary_key = True, 
        default = uuid.uuid4, 
        editable =False
        )
    society_name = models.CharField(
        max_length = 300, 
        blank = False, 
        null = False
        )
    address = models.TextField(
        blank = False, 
        null = False
        )
    admin = models.OneToOneField(
        User, 
        related_name = 'admin_of'
        )

    class Meta:
        verbose = 'Societies'

    def __str__(self):
        return self.society_name 

    def __unicode__(self):
        return self.society_name + " " +  self.society_id




#Class for housing units in a society.
class LivingUnit(models.Model):

    '''
        Database model for houses in a particular society.
    '''

    unit_id = models.UUIDField(
        primary_key = True , 
        default = uuid.uuid4, 
        editable = False
    )
    unit_number = models.CharField(
        max_length = 100, 
        blank = False, 
        null = False
    )
    parent_society = models.ForeignKey(
        Society,
        related_name = 'living_units'
    )

    class Meta:
        verbose = 'LivingUnits'

    def __str__(self):
        return self.parent_society+" "+self.unit_id

    def __unicode__(self):
        return self.parent_society+" "+self.unit_id