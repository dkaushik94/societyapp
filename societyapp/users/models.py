from django.db import models
from django.contrib.auth.models import AbstractUser


#Author: Debojit Kaushik ( 25/3/2017 )


class User(AbstractUser):
    
    '''
        Database model for Base Custom User. 
        Can be extended if necessary by inheriting.
    '''

    GENDER_CHOICES  =   (
        ('Male','Male'),
        ('Female','Female'),
        ('Other','Other'),
    )

    user_id   =   models.UUIDField(
        primary_key = True, 
        default=uuid.uuid4, 
        editable=False
    )
    first_name  =   models.CharField(
        max_length = 100, 
        blank = False, 
        null = False
    )
    middle_name =   models.CharField(
        max_length = 100, 
        blank = True, 
        null = False
    )
    last_name = models.CharField(
        max_length = 100, 
        blank = True, 
        null = False
    )
    gender = models.CharField(
        max_length = 5, 
        choices = GENDER_CHOICES
    )
    primary_contact_number = PhoneNumberField(
        blank = False, 
        null = False
    )
    secondary_contact_number = PhoneNumberField(
        blank = True, 
        null = True
    )
    landline_number = PhoneNumberField(
        blank = True, 
        null=True
    )
    intercom_number = models.CharField(
        blank = True, 
        null= True
    )
    apartment_number = models.CharField(
        max_length = 10 ,
        blank = False, 
        null = False
    )
    profile_pic = models.URLField(
        blank = True
    )
    related_society = models.ForeignKey(
        Society, 
        related_name = 'residents'
    )
    living_unit = models.ForeignKey(
        LivingUnit,
        related_name = 'residents'
    )
    

    class Meta:
        verbose_name    =   'Users'

    class __str__(self):
        return self.user_id

    class __unicode__(self):
        return self.first_name + " " + self.last_name + " " + self.user_id