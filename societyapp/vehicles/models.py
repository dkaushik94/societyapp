from django.db import models

# Create your models here.

#Author : Debojit Kaushik ( 26/3/2017 )

#Class for vehicles.
class Vehicle(models.Model):

    '''
        Database model for Vehicle.
    '''

    COLORS = (
        ('red','RED'),
        ('blue','GREEN'),
        ('green','GREEN'),
        ('yellow','YELLOW'),
        ('white','WHITE'),
        ('black','BLACK'),
        ('grey','GREY'),
        ('silver','SILVER'),
        ('golden','GOLDEN'),
        ('beige','BEIGE')
    )

    vehicle_id = models.UUIDFIeld(
        primary_key = True, 
        default = uuid.uuid4 , 
        editable = False
    )
    owner = models.ForeignKey(
        User,
        related_name  = 'vehicles' 
    )
    color =  models.CharField(
        max_length = 20,
        choices = COLORS 
    )
    model = models.CharField(
        max_length = 100,
        blank = False,
        null = False, 
    )
    registration_number = models.CharField(
        max_length = 12, 
        blank = False, 
        null = False
    )

    class Meta:
        verbose = 'Vehicles'

    def __str__(self):
        return self.vehicle_id

    def __unicode__(self):
        return self.color+" "+self.vehicle_id